#!/bin/bash

# This script is meant to publish the current site to the public_html folder
# that's being served out to the internet. This has to be done because of
# directory ownership issues in the /var/www directory.
#
# This script must be run with sudo to execute successfully.

# Variables
hostname='cinnamon.shew.io'
src_site_dir='site'
tar_site_dir="/var/www/${hostname}/public_html"
message_prefix='[DemoSitePub] '

# Checking for necessary preconditions
if [ $EUID -ne 0 ]; then
	echo "This program must either be run with sudo or as root"
	echo "ABORTING PUBLISH"
	exit
fi


if [ "$(type -t nginx)" != "file" ]; then
	echo "The nginx service doesn't seem to be installed"
	echo "To install this service run: sudo apt install nginx"
	echo "ABORTING PUBLISH"
	exit
fi

if [ ! -d ${src_site_dir} ]; then
	echo "The 'site' directory doesn't exist in the current directory"
	echo "ABORTING PUBLISH"
	exit
fi

if [ ! -d ${tar_site_dir} ]; then
	echo "The target publishing directory doesn't exist"
	echo "ABORTING PUBLISH"
	exit
fi

# Stop the Nginx service before proceeding
echo "${message_prefix}Stopping Nginx service..."
service nginx stop
echo "${message_prefix}Done!"

# Replace the directory being served with the this one
echo "${message_prefix}Replacing existing site directory..."
rm -rf ${tar_site_dir}
cp -r ${src_site_dir} ${tar_site_dir}
echo "${message_prefix}Done!"

# Fix permissions on the directory being served
echo "${message_prefix}Fixing site directory permissions..."
chown -R www-data:www-data "${tar_site_dir}"
chmod 755 ${tar_site_dir}
echo "${message_prefix}Done!"

# Restart the nginx service
echo "${message_prefix}Restarting Nginx service..."
service nginx start
echo "${message_prefix}Done!"

echo "${message_prefix}FINISHED PUBLISHING SITE"
