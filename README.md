# Cookie Engine

A 3D web engine for viewing models in a first-person perspective.

## Purpose

This project is meant to be presented for a Principles of Computer Graphics course.
The specifications loosely put are:

* Enable loading of `.obj` files to render and interact with within the engine
* Implement input control stream to allow users to move the camera around
* Simulate physical properties like collision detection and transformations
* Support lighting simulations with vertex shaders with editable light sources

If the project is finished and the code is relatively maintainable, then this
code will serve as a jumping off point to create a more feature reach 3D web
engine that will allow for 3D web games that support server side logic
handling with client side graphics rendering with a message bus for control
inputs and game commands.

More info on the status of the project will be available as further iterations
are merged into the master branch.

## Contact

| Roles | Name | Contact Info |
| --- | --- | --- |
| Repo Owner, Developer, Project Owner | Dan A. Herrera | dherr060@fiu.edu |

