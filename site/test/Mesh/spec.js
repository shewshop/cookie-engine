describe("Mesh", function() {
  var mesh;
  var objFileString;

  beforeEach(function() {
    objFileString = 'o my_cube.obj\n' +
            'v 1 1 1\n' +
            'v -1 1 1\n' +
            'v -1 -1 1\n' +
            'v 1 -1 1\n' +
            'v 1 1 -1\n' +
            'v -1 1 -1\n' +
            'v -1 -1 -1\n' +
            'v 1 -1 -1\n' +
            'vn 0 0 1\n' +
            'vn 1 0 0\n' +
            'vn -1 0 0\n' +
            'vn 0 0 -1\n' +
            'vn 0 1 0\n' +
            'vn 0 -1 0\n' +
            'f 1//1 2//1 3//1\n' +
            'f 3//1 4//1 1//1\n' +
            'f 5//2 1//2 4//2\n' +
            'f 4//2 8//2 5//2\n' +
            'f 2//3 6//3 7//3\n' +
            'f 7//3 3//3 2//3\n' +
            'f 7//4 8//4 5//4\n' +
            'f 5//4 6//4 7//4\n' +
            'f 5//5 6//5 2//5\n' +
            'f 2//5 1//5 5//5\n' +
            'f 8//6 4//6 3//6\n' +
            'f 3//6 7//6 8//6'
    mesh = new Mesh(objFileString);
  });

  it("should load a mesh correctly from an obj string", function() {
    expect(mesh).not.toBe(null);
    expect(mesh.vertices).not.toBe(null);
    expect(mesh.indices).not.toBe(null);

    var numLoaded = {};
    // 3 coordinates for each unique face vertex
    numLoaded.vertices = mesh.vertices.length/(3);
    // Number of unique face vertices
    numLoaded.indices = mesh.indices.length;

    expect(numLoaded.vertices).toEqual(24);
    expect(numLoaded.indices).toEqual(36);

    var lastLoaded = {};
    lastLoaded.vertex = mesh.vertices.slice(-3);
    lastLoaded.index = mesh.indices[mesh.indices.length-1];

    // Last vertex triple pointed to by last unique index triple
    expect(lastLoaded.vertex).toEqual([-1,-1,-1]);
    // First index of last loaded index triple
    expect(lastLoaded.index).toEqual(20);
  });
});
