describe("Camera", function() {
  var camera;

  beforeEach(function() {
    camera = new Camera();
  });

  describe("when constructed, should be", function() {
    it("centered at the world origin", function() {
      expect(camera.translation).toEqual(vec3.fromValues(0,0,0));
    });

    it("facing into the -Z axis", function() {
      expect(camera.pitch).toEqual(0);
      expect(camera.yaw).toEqual(270);
    });
  });

  describe("should translate correctly with", function() {
    var initialPosition, directionVec, distance,
        alpha, beta,expectedPosition; 

    it("a random vector", function() {
      initialPosition = vec3.fromValues(-2, -5, 0);
      directionVec = vec3.fromValues(3, 4, 5);
      distance = 12;
      expectedPosition = vec3.fromValues(3.0911688,
                                         1.7882251,
                                         8.4852814);

      camera.translation = initialPosition;
      camera.translate(directionVec, distance);

      for (var i = 0; i < 3; i++) {
        expect(camera.translation[i]).toBeCloseTo(expectedPosition[i]);
      }
    });

    it("its forward vector", function() {
      alpha = 45;
      beta = 180;
      initialPosition = vec3.fromValues(1, 0, -1);
      distance = 10;
      expectedPosition = vec3.fromValues(1.0000000,
                                         7.0710678,
                                         6.0710678);

      camera.pitch += alpha;
      camera.yaw += beta;
      camera.translation = initialPosition;
      camera.translate(camera.forwardVector(), distance);

      for (var i = 0; i < 3; i++) {
        expect(camera.translation[i]).toBeCloseTo(expectedPosition[i]);
      }
    });
  });

  describe("should correctly calculate", function() {
    var alpha, beta, directionVec, distance,
        fovY, width, height, near, far,
        expectedVector, expectedMatrix,
        calculatedVector, calculatedMatrix;

    it("its right vector", function() {

      beta = -25;
      expectedVector = vec3.fromValues( 0.9063078,
                                        0.0000000,
                                       -0.4226183);

      camera.yaw += beta;

      calculatedVector = camera.rightVector();
      for (var i = 0; i < 3; i++) {
        expect(calculatedVector[i]).toBeCloseTo(expectedVector[i]);
      }
    });

    it("its forward vector", function() {
      alpha = 40;
      beta = -25;
      expectedVector = vec3.fromValues(-0.3237444,
                                        0.6427876,
                                       -0.6942720);

      camera.pitch += alpha;
      camera.yaw += beta;

      calculatedVector = camera.forwardVector();
      for (var i = 0; i < 3; i++) {
        expect(calculatedVector[i]).toBeCloseTo(expectedVector[i]);
      }
    });

    it("its view matrix", function() {
      directionVec = vec3.fromValues(1, 1, 0);
      distance = 2;
      alpha = 45;
      beta = 90;
      expectedMatrix = mat4.fromValues( 0.0000, -0.7071,  0.7071, 0,
                                        0.0000,  0.7071,  0.7071, 0,
                                       -1.0000,  0.0000,  0.0000, 0,
                                        0.0000,  0.0000, -2.0000, 1);

      camera.translate(directionVec, distance);
      camera.pitch += alpha;
      camera.yaw += beta;

      calculatedMatrix = camera.viewMatrix();
      for (var i = 0; i < 16; i++) {
        expect(calculatedMatrix[i]).toBeCloseTo(expectedMatrix[i]);
      }
    });

    it("a projection matrix", function() {
      width = 800; height = 600;
      fovY = 45; near = 1.0; far = 1000.0;
      expectedMatrix = mat4.fromValues( 1.8107,  0.0000,  0.0000,  0.0000,
                                        0.0000,  2.4142,  0.0000,  0.0000,
                                        0.0000,  0.0000, -1.0020, -1.0000,
                                        0.0000,  0.0000, -2.0020,  0.0000);
          
      calculatedMatrix = camera.projectionMatrix(fovY, width, height, near, far);
      for (var i = 0; i < 16; i++) {
        expect(calculatedMatrix[i]).toBeCloseTo(expectedMatrix[i]);
      }
    });
  });
});
