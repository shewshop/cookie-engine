// When created, the camera is at the world origin, looking
// in the -Z-axis.
// +pitch is counter-clockwise on the ZY-plane from the +X-axis
// +yaw is clockwise on the XZ-plane from the +Y-axis
//
// Within this module, alpha, beta, and theta will correspond to
// clockwise rotations around the X-axis, Y-axis, and some general
// point in space
function Camera() {
  this.pitch = 0;
  this.yaw = 270;
  this.translation = vec3.fromValues(0,0,0);
}

Camera.prototype.translate = function(directionVec, dist) {
  var transVec = vec3.fromValues(0,0,0);

  vec3.normalize(transVec, directionVec);
  vec3.scale(transVec, transVec, dist);

  vec3.add(this.translation, this.translation, transVec);
}

Camera.prototype.rightVector = function() {
  var beta, x, y, z;

  beta = (this.yaw + 90.0) * Math.PI / 180.0;
  x = Math.cos(beta);
  y = 0;
  z = Math.sin(beta);

  return vec3.fromValues(x, y, z);
}

Camera.prototype.forwardVector = function() {
  var alpha, beta, x, y, z;

  alpha = this.pitch * Math.PI / 180.0;
  beta = this.yaw * Math.PI / 180.0;
  x = Math.cos(alpha) * Math.cos(beta);
  y = Math.sin(alpha);
  z = Math.cos(alpha) * Math.sin(beta);

  return vec3.fromValues(x, y, z);
}

Camera.prototype.viewMatrix = function() {
  var alpha, beta, cosA, cosB, sinA, sinB,
      x, y, z, dx, dy, dz, t, _translation,
      extrinsicMatrix, result;

  // Here we precompute a few values that will be used repeatedly
  alpha = this.pitch * Math.PI / 180.0;
  beta = (270.0 - this.yaw) * Math.PI / 180.0;
  cosA = Math.cos(alpha); sinA = Math.sin(alpha);
  cosB = Math.cos(beta); sinB = Math.sin(beta);

  x = vec4.fromValues(cosB, 0, sinB, 0);
  y = vec4.fromValues(sinA * sinB, cosA, -sinA * cosB, 0);
  z = vec4.fromValues(-cosA * sinB, sinA, cosA * cosB, 0);
  t = vec3.create(); vec3.negate(t, this.translation);
  _translation = vec4.fromValues(t[0], t[1], t[2], 1);
  dx = vec4.dot(x, _translation);
  dy = vec4.dot(y, _translation);
  dz = vec4.dot(z, _translation);

  result = mat4.fromValues( x[0], y[0], z[0], 0,
                            x[1], y[1], z[1], 0,
                            x[2], y[2], z[2], 0,
                              dx,   dy,   dz, 1);

  return result;
}

Camera.prototype.projectionMatrix = function(fovY, width, height, near, far) {
  var theta, ratio, result;

  theta = fovY * Math.PI / 180.0;
  ratio = 1.0 * width / height;
  result = mat4.create();
  mat4.perspective(result, theta, ratio, near, far);

  return result;
}

