# To Do

This is a list used to track features and changes that remain to be implemented

- ~~Create and setup repo with initial files~~
- ~~Implement Javascript `.obj` loader~~
- ~~Refactor `.obj` loader for increased legibility~~
- Create basic HTML page that will be used for demo pages
- Design more tests for `.obj` loader
- Implement basic shaders that will be used to render items (possibly as wireframe models for now)
- Decide on and implement Physical data-strucutre and general implementation for world
- Implement a moveable camera that uses the mouse to change the point it is focused on
- Implement collision detection for the camera entity along with flying and jumping movements
- Add lighting calculations to the vertex shader that's being used
- Add interface that will allow users to add light sources into the world

